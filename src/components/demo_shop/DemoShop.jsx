import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import coca from "../../img/coca.jpg";
import pepsi from "../../img/pepsi.jpg";
import sting from "../../img/sting.jpg";
import milk from "../../img/milk.jpg";
import DemoButton from "./DemoButton";

class DemoShop extends React.Component {
  constructor() {
    super();
    
    this.state = {
      data:[
        {
          id: 1,
          name: "Coca Cola",
          isSelect: false,
          img:coca,
        },
        {
          id: 2,
          name: "Pepsi",
          isSelect: false,
          img:pepsi,
        },
        {
          id: 3,
          name: "Sting",
          isSelect: false,
          img:sting,
        },
        {
          id: 4,
          name: "Milk",
          isSelect: false,
          img:milk,
        },
      ]
    };
  }

  updateSelected = (id, name, img, isSelect) => {
    this.setState({
      data: this.state.data.map((newData) =>
        newData.id === id ? { id, name, img ,isSelect } : newData,
      ),
    });
  }

  render() {
    return (
      <div className="container">
        <div className="select-product-option">
          <h1>Welcome to My Shop</h1>
          <table className="table table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Action</th>
              </tr>
              {this.state.data.map((item) => {
                return (
                  <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                      <Button
                        variant={item.isSelect ? "success" : "danger"} 
                        onClick={() => {
                          this.updateSelected(item.id, item.name,item.img, !item.isSelect);
                        }
                      }
                      >{item.isSelect ? "select" : "unselect"}</Button>
                    </td>
                  </tr>
                );
              })}
            </thead>
            <tbody></tbody>
          </table>
        </div>

        <div className="select-product-option">
          <div className="card-group">
            {this.state.data.map((cardItem) => {
              return (
                <div className="card text-center">
                  <img className="card-img-top" src={cardItem.img} />
                  <div className="card-body">
                    <p className="card-text">{cardItem.id}</p>
                    <p className="card-text">{cardItem.name}</p>
                    <DemoButton variant={cardItem.isSelect ? "success" : "danger"} label={cardItem.isSelect ? "select" : "unselect"} />
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default DemoShop;
