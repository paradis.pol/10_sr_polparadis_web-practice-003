import Button from "react-bootstrap/Button";
function DemoButton(props) {
    const label = props.label;
    const variant = props.variant;
    return (
      <Button variant={variant}>{label}</Button>
    );
}

export default DemoButton